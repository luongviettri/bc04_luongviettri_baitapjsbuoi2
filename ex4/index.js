function tinhHinhChuNhat() {
    var chieuDaiEl = document.getElementById("chieuDai").value * 1;
    var chieuRongEl = document.getElementById("chieuRong").value * 1;
    

    var dienTich = chieuDaiEl * chieuRongEl;
    var chuVi = (chieuDaiEl + chieuRongEl) * 2;

    var dienTichEl = document.getElementById("dienTich");
    var chuViEl = document.getElementById("chuVi");

    dienTichEl.innerHTML = `diện tích HCN là: ${dienTich}`;
    chuViEl.innerHTML = `chu vi HCN là: ${chuVi}`;
}

function xuLyEnter(e) {
    if (e.keyCode == 13) {
        tinhHinhChuNhat();
    }
}
