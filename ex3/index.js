

function changeMoney(e) {
    e.preventDefault();
    var USDEl = document.getElementById("USD").value * 1;

    var VnMoney = USDEl * 23500;

    var resultEl = document.getElementById("result");

    resultEl.innerHTML = new Intl.NumberFormat('vn-VN').format(VnMoney);
}

function pressEnter(e) {
    if (e.keyCode == 13) {
        changeMoney(e);
    }
}